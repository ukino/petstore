package router

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth"
	"github.com/ptflp/godecoder"
	"gitlab.com/ukino/petstore/internal/infrastructure/components"
	"gitlab.com/ukino/petstore/internal/infrastructure/responder"
	"go.uber.org/zap"
	"net/http"
)

func InitComponents() *components.Components {
	token := jwtauth.New("HS256", []byte("secret"), nil)
	users := make(map[int]string)
	logger := zap.NewExample()
	decoder := godecoder.NewDecoder()
	Responder := responder.NewResponder(decoder, logger)
	component := components.NewComponents(Responder, decoder, logger, token, users)
	return component
}

func NewRouter() http.Handler {
	r := chi.NewRouter()

	return r
}
