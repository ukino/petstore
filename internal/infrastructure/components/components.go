package components

import (
	"github.com/go-chi/jwtauth"
	"github.com/ptflp/godecoder"
	"gitlab.com/ukino/petstore/internal/infrastructure/responder"
	"go.uber.org/zap"
)

type Components struct {
	Responder responder.Responder
	Decoder   godecoder.Decoder
	Logger    *zap.Logger
	Token     *jwtauth.JWTAuth
	Users     map[int]string
}

func NewComponents(responder responder.Responder, decoder godecoder.Decoder, logger *zap.Logger, token *jwtauth.JWTAuth, users map[int]string) *Components {
	return &Components{
		Responder: responder,
		Decoder:   decoder,
		Logger:    logger,
		Token:     token,
		Users:     users,
	}
}
