package models

//go:generate easytags $GOFILE json

type Order struct {
	ID       int64  `json:"id"`
	PetID    int64  `json:"pet_id"`
	Quantity int32  `json:"quantity"`
	ShipDate string `json:"ship_date"`
	Status   string `json:"status"`
	Complete bool   `json:"complete"`
}
