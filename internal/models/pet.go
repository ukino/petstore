package models

//go:generate easytags $GOFILE json

type Pet struct {
	Id        int64    `json:"id"`
	Category  Category `json:"category"`
	Name      string   `json:"name"`
	PhotoUrls []string `json:"photo_urls"`
	Tags      []Tag    `json:"tags"`
	Status    string   `json:"status"`
}

type Category struct {
	Id   int64  `json:"id"`
	Name string `json:"name"`
}

type Tag struct {
	Id   int64  `json:"id"`
	Name string `json:"name"`
}
